import AWS from "aws-sdk";

const signed = async(event) => {  
    const json = event;
    try {
        const s3 = new AWS.S3({signatureVersion: 'v4'}); 

        if( json.PreSignUrl !== undefined){
            const bucketName  = json.PreSignUrl.bucketName;
            const keyName     = json.PreSignUrl.fileName;
            const folderName  = json.PreSignUrl.folderName;
            const contentType = json.PreSignUrl.contentType; 
            const method      = json.PreSignUrl.method;
            const expiresTime = json.PreSignUrl.expiresTime;
            const type        = json.PreSignUrl.method === "get" ? "getObject" : "putObject";
            
            const s3Params = {};
            
            if(method === "get") {
                s3Params.Bucket  = bucketName;
                s3Params.Key     = folderName + keyName;
                s3Params.Expires = expiresTime;
            } 
            else {
                s3Params.Bucket = bucketName;
                s3Params.Key = folderName + keyName;
                s3Params.Expires = expiresTime;
                s3Params.ContentType = contentType;
            }
        
            const signedUrl = await s3.getSignedUrlPromise(type, s3Params);

            return {
                statusCode: 200,
                body: signedUrl 
            };    
        }else if(json.ListObjects_v2 !== undefined){
            const bucketName = json.ListObjects_v2.bucketName; 
            const prefix = json.ListObjects_v2.Prefix; 
            
            const params = {
                Bucket: bucketName
            };
            
            if(prefix !== "")
                params.Prefix = prefix;
            
            const objList = await s3.listObjectsV2(params).promise();
            
            return {
                statusCode: 200,
                body: objList  
            };
        }
    } catch (error) {
        return {
            statusCode: error.statusCode,
            body: error.message
        };
    }
};
 
export const handler = signed;
